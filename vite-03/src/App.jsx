import ErrorContainer from './ErrorContainer'
import ClockIn from './ClockIn'
import './App.css'

function App() {
  
  return (
    <div className="App">
      <ErrorContainer>
        <ClockIn />
      </ErrorContainer>
    </div>
  )
}

export default App
