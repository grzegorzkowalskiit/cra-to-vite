import {BrowserRouter, Route, Routes } from "react-router-dom";
import Public from "./Public";
import Private1 from "./Private1";
import Private2 from "./Private2";
import Home from "./Home";
import SecurityProvider from "./SecurityProvider";
import SecureRoute from "./SecureRoute";

import './App.css';

function App() {

  return (
    <div className="App">
      <BrowserRouter>
        <SecurityProvider>
          <Routes>
            <Route exact path='/' element={<Home/>} />
            <Route exact path='/private1' element={<SecureRoute />}>
              <Route exact path='/private1' element={<Private1 />}/>
            </Route>
            <Route exact path='/private2' element={<SecureRoute />}>
              <Route exact path='/private2' element={<Private2 />}/>
            </Route>
            <Route exact path='/public' element={<Public/>} />
          </Routes>
        </SecurityProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;