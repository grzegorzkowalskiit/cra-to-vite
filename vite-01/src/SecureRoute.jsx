import Login from "./Login";
import { Outlet } from "react-router-dom";
import useSecurity from "./useSecurity";

const SecureRoute = (props) => {
    const {loggedIn} = useSecurity();

    return(
        loggedIn ? <Outlet /> : <Login />
    );
}

export default SecureRoute;