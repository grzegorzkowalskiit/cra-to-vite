import {useContext} from "react";
import SecurityContext from "./SecurityContext";

export default () => useContext(SecurityContext);